import java.time.DayOfWeek;
import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {

        System.out.println();
        friday13sInYear(2023);
        System.out.println();
        friday13sInYears(2022,2023);
    }

    /**
     * This method finds Friday the 13s in a given year, and when they occur.
     */
    public static void friday13sInYear(int yearInput)
    {
        int count = 0;

        System.out.println("Check Fridays in year " + yearInput);
        for (int i = 1; i <= 12; i++) // Go through the input year's months
        {
            LocalDate date = LocalDate.of(yearInput, i, 13); // Get every month's day number 13
            DayOfWeek dayOfWeek = date.getDayOfWeek();

            if (dayOfWeek == DayOfWeek.FRIDAY) // Check if the month's day number 13 is a Friday
            {
                count ++; // Count the number of Friday the 13s in the input year
                System.out.println("There is a Friday 13th in " + date.getMonth());
            }
        }
        System.out.println("Total Friday the 13's in " + yearInput + ": " + count);
    }

    /**
     * This method calculates the number of Friday the 13s in a given period of years.
     */
    public static void friday13sInYears(int yearStart, int yearFinish)
    {
        int count = 0;
        for (int i = yearStart; i <= yearFinish; i++) // Go through each year from yearStart to yearFinish
        {
            for (int j = 1; j <= 12; j++) // Get every month's day number 13
            {
                LocalDate date = LocalDate.of(i, j, 13);
                DayOfWeek dayOfWeek = date.getDayOfWeek();

                if (dayOfWeek == DayOfWeek.FRIDAY) // Check if the month's day number 13 is a Friday
                {
                    count ++; // Count the number of Friday the 13s from the start year to the finish year
                }
            }
        }
        System.out.println("Total Friday the 13's from " + yearStart + " to " + yearFinish + ": " + count);
    }
}
